# Design Document
---


## Contents
---
- [General](#general)<br>
   - [genre](#genre)  <br>
   - [Core mechanics](#core-mechanics)<br>
   - [Main idea](#main-idea)<br>
   - [Modes](#modes)<br>
   - [target platforms](#target-platforms)<br>
   - [language](#language)<br>

- [Technical Stack](#technical-stack)<br>
   - [Engine](#engine)<br>
   - [languages](#languages)<br>

- [Design and World](#design-and-world)<br>
   - [Setting](#setting)<br>
   - [Location design](#location-design)<br>
   - [Characters](#characters)<br>

- [Mechanics](#mechanics)<br>
   - [Rebellion](#rebellion)<br>
   - [Voltage scale](#voltage-scale)<br>
   - [Assimilation](#assimilation)<br>
   - [gathering and hunting](#gathering-and-hunting)<br>
   - [water-extraction](#water-extraction)<br>
   - [ore mining](#ore-mining)<br>
   - [war](#war)<br>
- [Balance](#balance)<br>


## General
---

### Genre 
2D pixel-art strategy <br>

### Core mechanics:
Managing objects and units on the game map. <br>

### Main idea
The gameplay takes place on a map consisting of 50x50 tiles. <br>
There are 4 civilizations on the map, the player has incomplete control over each of the <br> civilizations (the player has no control over some civilization functions). Each of the <br> civilizations expands and develops, by assimilating tiles on the map. A war may occur between <br> civilizations if two civilizations claim the same zone with resources. The goal of the game is to <br> maintain peace between civilizations for as  long as possible and prevent wars. <br>


### Modes
Single-player<br>


### Target platforms
Windows<br>


### Languages
English<br>


## Technical Stack
---
### Engine 
Defold <br>


### Language 
Lua<br>


## Design and World
---

### Setting
Events take place in an ordinary world familiar to all of us, with forests, mountains and rivers. <br> Human  civilizations are assimilating new lands, expanding and developing. However, the more <br> developed and large civilizations become, the greater the risk of war between them. Will the player <br> be able to maintain the balance of power and peace between civilizations without plunging them into <br> the abyss of war and death?<br>


### Location design
The game design is a pixel-art strategy. The game map consists of 50x50 tiles on which game actions <br> take place. The game map is filled with different zones from a cluster of tiles that have their own <br> functions: 
- Plain: ordinary tiles without functions, units walk on it and houses are built on them. Most of <br> the  game map consists of plains.


- Food zone - a zone of accumulation of tiles where units from civilizations can get food for <br> themselves. There are two types of food zones: plant and meat, civilizations can get food from any<br>  type of food zone. However, if two civilizations share the same food zone, it could lead to <br> competition for territory and later war.<br> 


- Source of water - these zones come in two types: rivers (line of tiles) and lakes (clusters of tiles). They perform the same functions as food zones.


- Mountains - are clusters of tiles. Extremely valuable for civilizations, by extracting ore from <br> the mountains, warrior units will begin to appear in civilizations <br>


### Characters
- Unit-worker - performs the following functions: building, gathering or hunting, ore mining, fighting<br>


- Unit-warrior - performs the following functions: fighting<br>


## Mechanics 
---


### Rebellion
An rebellion occurs when the voltage scale reaches its limit. After the rebellion, the player <br> completely loses control over the civilization and maintaining peace becomes more difficult, because <br> the rebel civilization will develop in its own interests.<br> 


### Voltage scale
There are three types of voltage scale: <br>
- Voltage scale when units are dissatisfied. This scale is filled when the player is inactive, when <br> the scale reaches its limit, a “rebellion” will occur. The scale will begin to decrease if the player<br> begins any actions with the management of the civilization.<br>

- Voltage scale during competition for a zone. This scale is filled when two civilizations use the <br> same resource zone. When this scale reaches its limit, a “war” occurs between two civilizations. The <br> scale will begin to decrease if one of the civilizations replaces the disputed resource zone with <br> another<br>

- Voltage scale if a civilization wants to capture an area with resources located on the territory <br> of an alien civilization. This happens when on the territory of a neighboring civilization there is <br> a zone with resources that the other civilization does not have, then the voltage scale is filled <br> and when the limit is reached, a “war” begins between the two civilizations. The scale will begin to<br>  decrease if the aggressing civilization has a zone with resources that a neighboring civilization has.<br>


### Assimilation
At the beginning of the game, each civilization has its own territory of 6 tiles with a town hall, <br> assimilation is the “capture” of a new zone of 6 tiles. The assimilated territory is distinguished by<br> a special border. Assimilation is completed after the building of a large house.<br>


### Gathering and hunting 
After a civilization has two assimilated territories, the units of the civilization will need food. <br> Food can be obtained in food zones, the player can choose which food zone to obtain food in (plant <br> or meat, gathering or hunting), depending on if there is one of the food zones near the territory of <br> civilization. If a food zone is located inside the territory of a civilization, then only that <br> civilization will be able to obtain food from this zone. Gathering and hunting is mandatory, if a <br> civilization is deprived of food for a long time, a “rebellion” will occur and the player will lose <br> control of the civilization.<br>


### Water extraction
Civilization units will need water after two assimilated territories. The system for obtaining water <br> is the same as for obtaining food. But unlike food, water zones are much larger and sparser.<br>
Water extraction is mandatory; if a civilization is deprived of water for a long time, a “rebellion” <br> will occur and the player will lose control of the civilization<br>


### Ore mining
Civilizations will be able to mine ores from the mountains after five assimilated territories. As <br> soon as a civilization begins mining ores, warrior units will appear in it. Mining of ores is not <br> required.<br>

### War
War begins when the voltage scale between two civilizations reaches its limit. During the war,<br> unit-workers attack nearby enemy units, and unit-warriors attack all enemy units and destroy <br> buildings (unit-workers cannot destroy buildings). If both civilizations have no unit-warriors left <br> and they cannot destroy houses, unit-workers begin to turn into unit-mersenary that destroy houses, <br> and are not inferior to unit-warriors in strength, after the end of the war, unit-mercenaries turn <br> into units-workers.<br>


## Balance
---


**soon...**